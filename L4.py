import matplotlib.pyplot as plt
import networkx as nx

class Wezel_drzewo:
    def __init__(self, data=None, left=None, right=None, previous=None):
        self.data = data
        self.left = left
        self.right = right
        self.previous = previous

    def __str__(self):
        return str(self.data)

class Wezel_lista_1k:
    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)

class Kolejka:
    def __init__(self):
        self.head = None
        self.tail = None

    def enqueue(self, data): #Dodawanie elementu do kolejki
        new_node = Wezel_lista_1k(data)
        if self.tail:
            self.tail.next = new_node
        self.tail = new_node
        if self.head is None:
            self.head = self.tail

    def dequeue(self): #Usuwanie elementu z kolejki
        if self.head is None:
            return None
        data = self.head.data
        self.head = self.head.next
        if self.head is None:
            self.tail = None
        return data

    def is_empty(self): #Czy jest pusta
        return self.head is None

class Stos:
    def __init__(self):
        self.top = None
        self._size = 0

    def is_empty(self):
        return self.top is None

    def push(self, data):
        new_node = Wezel_lista_1k(data)
        new_node.next = self.top
        self.top = new_node
        self._size += 1

    def pop(self):
        if self.is_empty():
            raise Exception("Pusty")
        data = self.top.data
        self.top = self.top.next
        self._size -= 1
        return data

    def size(self):
        return self._size


#Zadanie 1 - funkcje
def BFS(v): #W szerz
    Q = Kolejka()
    Q.enqueue(v)
    while not Q.is_empty():
        v = Q.dequeue()
        print(v, end=" ")
        if v.left:
            Q.enqueue(v.left)
        if v.right:
            Q.enqueue(v.right)

def DFS(v): #W głąb
    S = Stos()
    S.push(v)
    while not S.is_empty():
        v = S.pop()
        print(v, end=" ")
        if v.right:
            S.push(v.right)
        if v.left:
            S.push(v.left)

#Zadanie 2 - funckje
def DFS_2(v, kolejka):
    S = Stos()
    S.push(v)

    while not S.is_empty():
        v = S.pop()
        kolejka.enqueue(v)
        if v.right:
            S.push(v.right)
        if v.left:
            S.push(v.left)

def drzewo_z_liscia(leaf):
    def new_tree(wezel, Q):
        new_root = Wezel_drzewo(wezel.data)
        if not Q.is_empty():
            new_root.left = new_tree(Q.dequeue(), Q)
        if not Q.is_empty():
            new_root.right = new_tree(Q.dequeue(), Q)
        return new_root

    Q = Kolejka()
    Q.enqueue(leaf) #nowa kolejka nowego drzewa

    while leaf.previous: #Dopóki są wcześniejsze liście
        Q.enqueue(leaf.previous)
        if leaf.previous.left != leaf:
            DFS_2(leaf.previous.left, Q)
        if leaf.previous.right != leaf:
            DFS_2(leaf.previous.right, Q)
        leaf = leaf.previous
    return new_tree(Q.dequeue(), Q) #Tworzymy nowe drzewo na podstawie kolejki Q

#Zadanie 3 - funkcje
def drzewo_jako_graf(top, G=None, pos=None, x=0, y=0, layer_height=0.1):
    if G is None:
        G = nx.DiGraph()
    if pos is None:
        pos = {}
    if top is not None:
        pos[top] = (x, y)
        G.add_node(top)
        if top.left is not None:
            G.add_edge(top, top.left)
            pos = drzewo_jako_graf(top.left, G, pos, x=x - 1/layer_height, y=y - 1, layer_height=layer_height*2)
        if top.right is not None:
            G.add_edge(top, top.right)
            pos = drzewo_jako_graf(top.right, G, pos, x=x + 1/layer_height, y=y - 1, layer_height=layer_height*2)
    return pos

def rysuj_graf(root):
    G = nx.DiGraph()
    pos = drzewo_jako_graf(root, G)
    nx.draw(G, pos, with_labels=True, arrows=False)
    plt.show()

def BFS_2(v):
    Q = Kolejka()
    Q.enqueue((v, 0))
    current_level = -1
    licznik_wezlow = []
    licznik_lisci = 0
    poziomy_lisci = []

    while not Q.is_empty():
        v, lvl = Q.dequeue()

        if lvl > current_level:
            licznik_wezlow.append(0)
            current_level = lvl
        licznik_wezlow[lvl] += 1

        if v.left:
            Q.enqueue((v.left, lvl + 1))
        if v.right:
            Q.enqueue((v.right, lvl + 1))
        if not v.left and not v.right:
            licznik_lisci += 1
            poziomy_lisci.append((lvl, v))

    return licznik_wezlow, licznik_lisci, poziomy_lisci

def liscie_na_ost_poziomie(target_level, poziomy_lisci):
    print("\nLiscie na ost poziomie: ")
    for lvl in poziomy_lisci:
        if lvl[0] == target_level:
            print(lvl[1].data, end=" ")
            print()



#Zadanie 1 - dane
root = Wezel_drzewo('A')
root.right = Wezel_drzewo('B')
root.left = Wezel_drzewo('C')
root.left.right = Wezel_drzewo('D')
root.right.left = Wezel_drzewo('E')
root.left.left = Wezel_drzewo('F')
root.right.right = Wezel_drzewo('G')

#Zadanie 1a
BFS(root)
print("\n")

#Zadanie 1b
DFS(root)
print("\n")



#Zadanie 2 - dane
root = Wezel_drzewo('A', previous=None)
root.left = Wezel_drzewo('B', previous=root)
root.right = Wezel_drzewo('C', previous=root)
root.left.left = Wezel_drzewo('D', previous=root.left)
root.left.right = Wezel_drzewo('E', previous=root.left)
root.right.left = Wezel_drzewo('F', previous=root.right)
root.right.right = Wezel_drzewo('G', previous=root.right)
root.left.left.left = Wezel_drzewo('H', previous=root.left.left)
root.left.left.right = Wezel_drzewo('I', previous=root.left.left)
root.left.right.right = Wezel_drzewo('J', previous=root.left.right)
root.right.right.left = Wezel_drzewo('K', previous=root.right.right)
root.right.right.left.left = Wezel_drzewo('M', previous=root.right.right.left)
root.right.right.left.left.left = Wezel_drzewo('O', previous=root.right.right.left.left)
root.right.right.left.left.right = Wezel_drzewo('P', previous=root.right.right.left.left)
root.right.right.right = Wezel_drzewo('L', previous=root.right.right)
root.right.right.right.left = Wezel_drzewo('N', previous=root.right.right.right)

#Zadanie 2a
leaf = root.left.left.right
nowe_drzewo = drzewo_z_liscia(leaf)
BFS(nowe_drzewo)
print("\n")



#Zadanie 3 - dane
root = Wezel_drzewo('A', previous=None)
root.left = Wezel_drzewo('B', previous=root)
root.right = Wezel_drzewo('C', previous=root)
root.left.left = Wezel_drzewo('D', previous=root.left)
root.left.right = Wezel_drzewo('E', previous=root.left)
root.right.right = Wezel_drzewo('G', previous=root.right)
root.left.left.left = Wezel_drzewo('H', previous=root.left.left)
root.left.left.right = Wezel_drzewo('I', previous=root.left.left)
root.left.right.left = Wezel_drzewo('J', previous=root.left.right)
root.left.right.right = Wezel_drzewo('K', previous=root.left.right)
root.right.right.left = Wezel_drzewo('N', previous=root.right.right)

#Zadanie 3a
rysuj_graf(root)

#Zadanie 3b
ile_wezlow, liscie, poziomy_lisci = BFS_2(root)
for i, wezly in enumerate(ile_wezlow):
    print(f'poziom: {i} wezly: {wezly}')
print(f'liscie: {liscie} ')


#Zadanie 3c_1 -  najkrótsza ścieżka = poziom liścia
lvls = []
for lvl in poziomy_lisci:
    lvls.append(lvl[0])
print(f'\nNajkrótsza scieżka do liscia: {min(lvls)}')

#ZAdanie 3c_2
liscie_na_ost_poziomie(min(lvls), poziomy_lisci)
